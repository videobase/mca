<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'bd_mca');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'aNOh.l|gjPg^@/HVee|:/09WQ4g{n^:Q?qRwV[;bX*%0LjpT.^gmEeeV MMxA9)N');
define('SECURE_AUTH_KEY', 'Bsna{Mp-yCA+{j]6w;=LFk(pS=s)~idNikNfo^]#W=Z{ss[O3{Q}-zR{bHk>B&;i');
define('LOGGED_IN_KEY', 'T{47u|M:i|<RZr{7KNudR,8kiE,&k5n <h8vz3pAsweX%nhTQlqN*:NK_7Y@6%2[');
define('NONCE_KEY', 'K%DIBMA<<6JkvPLO`p/]p].P6-e4NA9D<H~ytr]VdEl,GU!;N{?IlUZ|Xkoc8*4`');
define('AUTH_SALT', 'C!-|`.<=gvJE&Pd#/0)Nqg}l&`([38Rv{43C>rQ/Hp<,:eMQ3c6I1o|Yh0ZAR#7A');
define('SECURE_AUTH_SALT', '/0Q<b41Jum5jS#caz>l s9*c^QwvJZ0d$z&EW2q*:g^k@/h)$Va<.7[KoMJXSL@_');
define('LOGGED_IN_SALT', '_CG(y{Q67! ;if**&Sto ]h8}ePaQO5(h+ri.:]@NgHXEe+{N}@I~?:t|C!PCs[|');
define('NONCE_SALT', '~5EK`0JbMrhZWT~D3SKbb2bInb*Xq4u]X_1~Ej^m=$(HDLt;GqWlv2W!F36P/8LT');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

